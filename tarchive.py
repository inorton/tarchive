"""
The tarchive module
"""

import tarfile
import subprocess
import platform

import os

GZIP = "gz"
BZIP = "bz2"
XZIP = "xz"


class TarchiveError(Exception):
    """
    Tarchive exception class
    """
    pass


class TarchiveUnsupported(TarchiveError):
    """
    Tarchive is not supported on this system, falling back to tarfile
    """
    pass


_TAR_PROGRAMS = []


def get_tar_program(basename="tar"):
    """
    Get a tar program for this system
    :return:
    """

    if _TAR_PROGRAMS:
        return _TAR_PROGRAMS[0]

    try:
        output = subprocess.check_output([basename, "--version"])
        if "GNU tar" in output:
            # check help output supports bzip2, xz and gzip
            output = subprocess.check_output([basename, "--help"])
            for option in ["--bzip2", "--xz", "--gzip"]:
                if option not in output:
                    return None
            _TAR_PROGRAMS.append(basename)
    except subprocess.CalledProcessError:
        pass

    if not _TAR_PROGRAMS:
        name = platform.system()
        if name == "Solaris" or name == "SunOS":
            return get_tar_program("gtar")
        return get_tar_program()
    raise TarchiveUnsupported()


def get_compress_option(compress):
    """
    Return the compress/decomress option
    :param compress:
    :return:
    """
    if compress == GZIP:
        return "--gzip"
    elif compress == XZIP:
        return "--xz"
    elif compress == BZIP:
        return "--bzip2"
    raise TarchiveUnsupported()


def get_tar_cmdline(filepath,
                    cwd,
                    compress,
                    verbose):
    if not cwd:
        cwd = os.getcwd()
    cmdline = [get_tar_program()]
    cmdline.extend(["-C", cwd])
    if compress:
        cmdline.append(get_compress_option(compress))
    if verbose:
        cmdline.append("-v")
    cmdline.extend(["-f", filepath])
    return cmdline


def create_tar(filepath, content,
               cwd=None,
               compress=GZIP,
               verbose=False,
               onefs=False
               ):
    """
    Create a tar archive
    :param filepath: archive to create
    :param content: files/folders to add
    :param cwd: excute tar from this folder
    :param compress: compressor to use
    :param verbose:
    :param onefs: do not cross filesystems
    :return:
    """
    cmdline = get_tar_cmdline(filepath, cwd, compress, verbose)
    if onefs:
        cmdline.append("--one-file-system")
    cmdline.append("-c")

    cmdline.extend(content)

    subprocess.check_call(cmdline, cwd=cwd, shell=False)


def unpack_tar(filepath, cwd, compress=None, verbose=False, preserve=False):
    """
    Unpack a tar archive. Relies on tar's auto detection
    :param preserve:
    :param verbose:
    :param compress: set to None for auto detect
    :param filepath:
    :param cwd:
    :return:
    """
    cmdline = get_tar_cmdline(filepath, cwd, compress, verbose)
    cmdline.append("-x")
    if preserve:
        cmdline.append("--preserve")

    subprocess.check_call(cmdline, cwd=cwd, shell=False)


def create(filepath, content,
           cwd=None,
           compress=GZIP,
           verbose=False,
           onefs=False):
    """
    Create a tar archive
    :param filepath: archive to create
    :param content: files/folders to add
    :param cwd: excute tar from this folder
    :param compress: compressor to use
    :param verbose:
    :param onefs: do not cross filesystems
    :return:
    """
    try:
        create_tar(filepath, content, cwd, compress, verbose, onefs)
    except TarchiveUnsupported:
        # fall back to tarfile
        mode = "w"
        if compress == XZIP:
            mode = "w:xz"
        elif compress == BZIP:
            mode = "w:bz2"
        elif compress == GZIP:
            mode = "w:gz"

        tar = tarfile.open(filepath, mode)
        try:
            for item in content:
                tar.add(item)
        finally:
            tar.close()


def unpack(filepath, cwd, compress=None, verbose=False, preserve=False):
    """
    Unpack a tar archive. Relies on tar's auto detection
    :param preserve:
    :param verbose:
    :param compress: set to None for auto detect
    :param filepath:
    :param cwd:
    :return:
    """
    try:
        unpack_tar(filepath, cwd, compress, verbose, preserve)
    except TarchiveUnsupported:
        # fall back to tarfile
        try:
            tar = tarfile.open(filepath)
            tar.extractall(path=cwd)
        finally:
            tar.close()
