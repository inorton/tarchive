# Tarchive

Tarchive is a python module for using tar with gzip and bz2
archives.

In most cases you will want to use the `tarfile` module. But,
if you simply want to pack or unpack archives and want to make
use of tools like `pigz` and take advantage of the fact that
GNU tar is pretty fast then this module might help you.

Tarchive falls back to the tarfile module if it can't find a
GNU tar
